import 'package:flutter/material.dart';

class ExerciseAppbar extends StatefulWidget {
  @override
  _ExerciseAppbarState createState() => _ExerciseAppbarState();
}

class _ExerciseAppbarState extends State<ExerciseAppbar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          children: <Widget>[
            Image.asset(
              'assets/images/facebook.png',
              fit: BoxFit.cover,
              height: 40,
            )
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: Image.asset('assets/images/search.png'),
            onPressed: () {},
          ),
          IconButton(
            icon: Image.asset('assets/images/msg.png'),
            onPressed: () {},
          )
        ],
      ),
      body: Container(
        decoration: BoxDecoration(color: Colors.grey),
      ),
    );
  }
}
