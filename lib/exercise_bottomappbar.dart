import 'package:flutter/material.dart';

class ExerciseBottomAppBar extends StatefulWidget {
  @override
  _ExerciseBottomAppBarState createState() => _ExerciseBottomAppBarState();
}

class _ExerciseBottomAppBarState extends State<ExerciseBottomAppBar> {
  PageController testPage = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: testPage,
        children: <Widget>[
          Center(
            child: Text('Page 1'),
          ),
          Center(
            child: Text('Page 2'),
          ),
          Center(
            child: Text('Page 3'),
          ),
          Center(
            child: Text('Page 4'),
          )
        ],
        physics: NeverScrollableScrollPhysics(),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.accessible),
                onPressed: () {
                  setState(() {
                    testPage.jumpToPage(0);
                  });
                },
                iconSize: 40,
              ),
              IconButton(
                icon: Icon(Icons.accessible),
                onPressed: () {
                  setState(() {
                    testPage.jumpToPage(1);
                  });
                },
              ),
              IconButton(
                icon: Icon(Icons.accessible),
                onPressed: () {
                  setState(() {
                    testPage.jumpToPage(2);
                  });
                },
              ),
              IconButton(
                icon: Icon(Icons.accessible),
                onPressed: () {
                  setState(() {
                    testPage.jumpToPage(3);
                  });
                },
              ),
            ],
          ),
        ),
        color: Colors.white,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {},
      ),
    );
  }
}
