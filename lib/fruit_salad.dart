import 'package:flutter/material.dart';

class FruitSalad extends StatefulWidget {
  @override
  _FruitSaladState createState() => _FruitSaladState();
}

class _FruitSaladState extends State<FruitSalad> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: <Widget>[
        // Container(
        //   color: Colors.white,
        // ),
        Padding(
          padding: EdgeInsets.all(20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(
                Icons.menu,
                color: Colors.black,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.bookmark,
                    color: Colors.black,
                  ),
                  SizedBox(width: 20.0),
                  Icon(
                    Icons.shopping_basket,
                    color: Colors.black,
                  )
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: Text('All Fruits',
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700)),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: Container(
            height: 400.0,
            width: 200.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Padding(
                  padding:
                      EdgeInsets.only(right: 15.0, top: 10.0, bottom: 20.0),
                  child: Container(
                    width: 250.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.yellow),
                    child: ListView(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(25.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Banana',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold)),
                              Row(
                                children: <Widget>[
                                  Text('\$10.00',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold)),
                                  Text(' (1kg)',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      )),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 200.0,
                          child: Image.asset('assets/images/banana.png'),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 70.0, right: 70.0, top: 15.0),
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              border: Border.all(color: Colors.white),
                            ),
                            child: Text('Detail',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 50.0, right: 50.0, top: 15.0),
                          child: Container(
                            height: 30.0,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey.withOpacity(0.4),
                            ),
                            child: Text('Add To Cart',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: 15.0, top: 10.0, bottom: 20.0),
                  child: Container(
                    width: 250.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.orange),
                    child: ListView(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(25.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Orange',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold)),
                              Row(
                                children: <Widget>[
                                  Text('\$5.00',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold)),
                                  Text(' (1kg)',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      )),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 200.0,
                          child: Image.asset('assets/images/orange.png'),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 70.0, right: 70.0, top: 15.0),
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              border: Border.all(color: Colors.white),
                            ),
                            child: Text('Detail',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 50.0, right: 50.0, top: 15.0),
                          child: Container(
                            height: 30.0,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey.withOpacity(0.4),
                            ),
                            child: Text('Add To Cart',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: 15.0, top: 10.0, bottom: 20.0),
                  child: Container(
                    width: 250.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.red),
                    child: ListView(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(25.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Strawberry',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold)),
                              Row(
                                children: <Widget>[
                                  Text('\$15.00',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold)),
                                  Text(' (1kg)',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      )),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 200.0,
                          child: Image.asset('assets/images/strawberry.png'),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 70.0, right: 70.0, top: 15.0),
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              border: Border.all(color: Colors.white),
                            ),
                            child: Text('Detail',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 50.0, right: 50.0, top: 15.0),
                          child: Container(
                            height: 30.0,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey.withOpacity(0.4),
                            ),
                            child: Text('Add To Cart',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
            height: 130.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RotatedBox(
                        quarterTurns: 3,
                        child: Text('Recipe',
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w700)),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 110.0,
                        width: 110.0,
                        child: ClipRRect(
                          borderRadius: new BorderRadius.circular(25.0),
                          child: Image.asset(
                            'assets/images/bananasalad.jpg',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Text('Banana Salad',
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 15.0))
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 110.0,
                        width: 110.0,
                        child: ClipRRect(
                          borderRadius: new BorderRadius.circular(25.0),
                          child: Image.asset(
                            'assets/images/mixfruitsalad.jpg',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Text('Mix Fruit Salad',
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 15.0))
                    ],
                  ),
                ),
              ],
            ))
      ],
    ));
  }
}
